(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, Version 2,
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)


val join_path : string -> string -> string

val expand_file : string -> string
val expand_open_in_ascii : string -> in_channel

val string_of_tm_mon : int -> string
val full_string_of_tm_mon : int -> string
val short_string_of_tm_wday : int -> string
val string_of_tm_wday : int -> string
val full_string_of_tm_wday : int -> string
val empty_tm : Unix.tm

val strip : string -> string
val read_all_shell_command_output : string -> string list * string list

val utf8_len : string -> int
val utf8_string_before : string -> int -> string
val utf8_string_after : string -> int -> string

val valid_env : string -> bool
